<%--
  Created by IntelliJ IDEA.
  User: 胡斌
  Date: 2021/12/15
  Time: 9:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>首页</title>

  <!-- 1. 导入CSS的全局样式 -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- 2. jQuery导入，建议使用1.9以上的版本 -->
  <script src="js/jquery-2.1.0.min.js"></script>
  <!-- 3. 导入bootstrap的js文件 -->
  <script src="js/bootstrap.min.js"></script>
  <script type="text/javascript">
  </script>
</head>
<body>
<div align="center">
   <h1>
     比特币系统设计大二上学期期末设计项目
   </h1>
  聚合查询
  <a
          href="${pageContext.request.contextPath}/indexServlet" style="text-decoration:none;font-size:33px">点击查询
  </a> </br>
  与比特币客户端实现通信
  <a
          href="${pageContext.request.contextPath}/bitcoinServlet" style="text-decoration:none;font-size:33px">点击通信
  </a> </br>

  随机生成 比特币地址
  <a
          href="${pageContext.request.contextPath}/BTCAddressServlet" style="text-decoration:none;font-size:33px">点我前往
  </a> </br>

</div>
</body>
</html>
