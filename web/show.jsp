<%--
  Created by IntelliJ IDEA.
  User: 胡斌
  Date: 2021/12/15
  Time: 9:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<!-- 网页使用的语言 -->
<html lang="zh-CN">
<head>
    <title>Title</title>
</head>
<body>

<form>
    <!-- 遍历结果集-->
    <c:forEach items="${hotvideo}" var="video" varStatus="s">
        <tr>
            <td>
                <div>
                    <img src="${video.item_cover}">
                </div>
            </td>
            <td>
                ${video.title}
            </td>
            <td>
                <a href="${video.share_url}">点击跳转</a>
            </td>
        </tr>
    </c:forEach>
</form>

</body>
</html>
