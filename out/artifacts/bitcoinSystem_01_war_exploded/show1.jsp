<%--
  Created by IntelliJ IDEA.
  User: 胡斌
  Date: 2021/12/16
  Time: 16:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form>
    <!-- 遍历结果集-->
    <c:forEach items="${games}" var="mygames" varStatus="s">
        <tr>
            <td>
                <div>
                    <img src="${mygames.avatar}">
                </div>
            </td>
            <td>
                    ${mygames.nickname}
            </td>
        </tr>
    </c:forEach>

</form>

</body>
</html>
