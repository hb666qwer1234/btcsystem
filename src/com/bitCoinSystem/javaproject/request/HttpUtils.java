package com.bitCoinSystem.javaproject.request;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Set;

/**
 * @author: nilcity
 *  该类用于 发起 Http网络请求，并且网络请求的结果数据返回
 */
public class HttpUtils {
    /**
     *
     * @param path
     * @return 返回结果集
     *  该函数用于 Get
     */
    public static String Get(String path){
        // 定义一个总结果 Result
        String Result="";
        String line = "";
        BufferedReader bufferedReader=null;
        InputStreamReader inputStreamReader=null;
        InputStream inputStream=null;
        try {
            URL url = new URL(path);
            URLConnection urlConn = url.openConnection();
            urlConn.connect();
            // 获取 连接数据流
            inputStream= urlConn.getInputStream();
            // 由于 字符流只能 按字符读 于是需要转换 为 字节流按行读取
             inputStreamReader = new InputStreamReader(inputStream,"UTF-8");
             bufferedReader = new BufferedReader(inputStreamReader);
            while ((line =bufferedReader.readLine()) != null){
                Result += line;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
                try {
                    if(bufferedReader != null){
                        bufferedReader.close();
                    }
                    if (inputStreamReader != null){
                        inputStreamReader.close();
                    }
                    if (inputStream != null){
                        inputStream.close();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
                return Result;
        }
    }

    /**
     * 该方法用于实现 Post 请求
     * @param path
     * @param header
     * @param data
     * @return
     * @throws IOException
     */
    public static String post(String path, Map<String,String> header,byte[] data) throws IOException {
        BufferedReader bufferedReader=null;
        String Result="";
        OutputStream outputStream=null;
        try {
            URL url = new URL(path);
           HttpURLConnection httpURLConnection= (HttpURLConnection) url.openConnection();
           // 设置为Post 请求
           httpURLConnection.setRequestMethod("POST");
           // 允许接受数据
            httpURLConnection.setDoInput(true);
            // 允许发送数据
            httpURLConnection.setDoOutput(true);

            // 设置请求头
            // contentType : application/json
            // Authorization : "Basic "+base64(用户名＋密码)
            // Property : 属性
//            httpURLConnection.setRequestProperty("ContentType","application/json");
//            httpURLConnection.setRequestProperty("Authorization","Basic "+"XXX");
            if (!header.isEmpty()) {
                // 如果请求头不为空， 那么将用户设置的属性设置到请求头中
                // 遍历 Map
                // 拿到所有的key
                Set<String> Keys = header.keySet();
                System.out.println(Keys);
                // 遍历Set集合
                System.out.println(header.size());
                for (String key : Keys) {
                    // 根据 Key 来取Value
                    String value = header.get(key);
                    System.out.println("key:"+key+":"+"value:"+value);
                    // 将拿到的 键值对放入
                    httpURLConnection.setRequestProperty(key,value);
                }


                // 建立连接
                httpURLConnection.connect();
                // 输出流
                 outputStream = httpURLConnection.getOutputStream();
                // 将数据发送出去
                outputStream.write(data);

                outputStream.flush();
                // 转换流
                BufferedReader bufferedReader1 = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                String line=null;
                while ((line=bufferedReader1.readLine())!=null){
                    Result +=line;
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (bufferedReader != null){
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null){
                outputStream.close();
            }
        }
        return Result;
    }
}
