package com.bitCoinSystem.javaproject.Bean;

/**
 * @author: nilcity
 */
public class Rpcbitcoin {
    public String Result;
    public String error;
    public int id;

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
