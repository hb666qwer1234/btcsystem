package com.bitCoinSystem.javaproject.Bean;

import java.util.List;

/**
 * @author: nilcity
 */
public class GameCommon {
    public String reason;
    public List<Game> Result;
    public int errorcode;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<Game> getResult() {
        return Result;
    }

    public void setResult(List<Game> result) {
        Result = result;
    }

    public int getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(int errorcode) {
        this.errorcode = errorcode;
    }
}
