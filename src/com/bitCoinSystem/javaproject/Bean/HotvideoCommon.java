package com.bitCoinSystem.javaproject.Bean;

import java.util.List;

/**
 * @author: nilcity
 * 用于 封装 热门视频榜单类
 */

public class HotvideoCommon {
    public String reason;
    public List<Hotvideo> Result;
    public int errorcode;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<Hotvideo> getResult() {
        return Result;
    }

    public void setResult(List<Hotvideo> result) {
        Result = result;
    }

    public int getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(int errorcode) {
        this.errorcode = errorcode;
    }
}
