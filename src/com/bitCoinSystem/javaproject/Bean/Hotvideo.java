package com.bitCoinSystem.javaproject.Bean;

/**
 * @author: nilcity
 *  热门视频内部 json 对象
 */
public class Hotvideo {
    public String title;
    public String share_url;
    public String author;
    public String item_cover;
    public int hot_value;
    public String hot_words;
    public int play_count;
    public int item_digg_countcover;
    public int comment_count;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShare_url() {
        return share_url;
    }

    public void setShare_url(String share_url) {
        this.share_url = share_url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getItem_cover() {
        return item_cover;
    }

    public void setItem_cover(String item_cover) {
        this.item_cover = item_cover;
    }

    public int getHot_value() {
        return hot_value;
    }

    public void setHot_value(int hot_value) {
        this.hot_value = hot_value;
    }

    public String getHot_words() {
        return hot_words;
    }

    public void setHot_words(String hot_words) {
        this.hot_words = hot_words;
    }

    public int getPlay_count() {
        return play_count;
    }

    public void setPlay_count(int play_count) {
        this.play_count = play_count;
    }

    public int getItem_digg_countcover() {
        return item_digg_countcover;
    }

    public void setItem_digg_countcover(int item_digg_countcover) {
        this.item_digg_countcover = item_digg_countcover;
    }

    public int getComment_count() {
        return comment_count;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }
}
