package com.bitCoinSystem.javaproject.Bean;

/**
 * @author: nilcity
 */
public class Game {
    public String nickname;
    public int follower_count;
    public int effect_value;
    public String avatar;
    public String[] video_list;
    public String item_cover;
    public String share_url;
    public String title;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getFollower_count() {
        return follower_count;
    }

    public void setFollower_count(int follower_count) {
        this.follower_count = follower_count;
    }

    public int getEffect_value() {
        return effect_value;
    }

    public void setEffect_value(int effect_value) {
        this.effect_value = effect_value;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String[] getVideo_list() {
        return video_list;
    }

    public void setVideo_list(String[] video_list) {
        this.video_list = video_list;
    }

    public String getItem_cover() {
        return item_cover;
    }

    public void setItem_cover(String item_cover) {
        this.item_cover = item_cover;
    }

    public String getShare_url() {
        return share_url;
    }

    public void setShare_url(String share_url) {
        this.share_url = share_url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
