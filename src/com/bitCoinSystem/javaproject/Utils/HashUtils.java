package com.bitCoinSystem.javaproject.Utils;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

/**
 * @author: nilcity
 *  生成 Hash 算法
 */


public class HashUtils {
    /**
     *
     * @param pubkey
     * @return Hash 值
     * @throws NoSuchAlgorithmException
     */
    public static  byte[] SHA256(byte[] pubkey)  {

        try {
          MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.update(pubkey);
            return digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] RIPEMD160(byte[] data){
        Security.addProvider(new BouncyCastleProvider());
        try {
            MessageDigest digest =MessageDigest.getInstance("RipeMD160");
            digest.update(data);
            return digest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
