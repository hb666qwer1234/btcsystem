package com.bitCoinSystem.javaproject.Service.Commonrpc;

import com.bitCoinSystem.javaproject.Constants;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: nilcity
 */
public class Commonrpc {
    public static Map<String, String> getheader(){
        // 拼接
        String USER = Constants.BITCOIN_RPC_USER+ ":" +Constants.BITCOIN_RPC_PASSWORLCD;
        // base64 编码
        Base64.Encoder encoder = Base64.getEncoder();
        String authbase64 = encoder.encodeToString(USER.getBytes());
        // 准备请求头
        Map<String, String> header = new HashMap<>();
        header.put("ContentType","application/json");
        header.put("Authorization","Basic "+authbase64);
        return header;
    }
}
