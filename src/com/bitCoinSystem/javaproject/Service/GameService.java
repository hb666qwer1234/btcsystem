package com.bitCoinSystem.javaproject.Service;

import com.alibaba.fastjson.JSONObject;
import com.bitCoinSystem.javaproject.Bean.Game;
import com.bitCoinSystem.javaproject.Bean.GameCommon;
import com.bitCoinSystem.javaproject.Bean.Hotvideo;
import com.bitCoinSystem.javaproject.Bean.HotvideoCommon;
import com.bitCoinSystem.javaproject.Constants;
import com.bitCoinSystem.javaproject.request.HttpUtils;

import java.util.List;

/**
 * @author: nilcity
 */
public class GameService {
    /**
     * 聚合查询功能
     *
     * @return
     */
    public static List<Game> GetGameService() {
        String url = Constants.GAME_URl + "?key=" + Constants.Key + "&type=game_console" + "&size=20";
        String Result = HttpUtils.Get(url);
        System.out.println(Result);

        //  JSON 解析
       GameCommon gameCommon = JSONObject.parseObject(Result, GameCommon.class);
        List<Game> result = gameCommon.getResult();
        return result;
    }
}