package com.bitCoinSystem.javaproject.Service;

import com.alibaba.fastjson.JSONObject;
import com.bitCoinSystem.javaproject.Bean.Hotvideo;
import com.bitCoinSystem.javaproject.Bean.HotvideoCommon;
import com.bitCoinSystem.javaproject.Constants;
import com.bitCoinSystem.javaproject.request.HttpUtils;

import java.util.List;

/**
 * @author: nilcity
 */
public class HotvidaoService {

     /**
      *  聚合查询功能
      * @return
      */
     public static List<Hotvideo> GetHotvidaoService(){
     String url = Constants.VIDEO_URl +"?key=" +Constants.Key+"&type=hot_video"+"&size=20";
     String Result = HttpUtils.Get(url);
     System.out.println(Result);

     //  JSON 解析
     HotvideoCommon hotvideoCommon = JSONObject.parseObject(Result, HotvideoCommon.class);
     List<Hotvideo> result = hotvideoCommon.getResult();

     return result;
 }
}