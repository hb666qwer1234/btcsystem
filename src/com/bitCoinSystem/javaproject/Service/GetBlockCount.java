package com.bitCoinSystem.javaproject.Service;

import com.alibaba.fastjson.JSONObject;
import com.bitCoinSystem.javaproject.Bean.Rpcbitcoin;
import com.bitCoinSystem.javaproject.Constants;
import com.bitCoinSystem.javaproject.Service.Commonrpc.Commonrpc;
import com.bitCoinSystem.javaproject.request.HttpUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: nilcity
 */
public class GetBlockCount {
    public static Rpcbitcoin getblockcount(){
        // 调用 公共的代码
        Map<String, String> header = Commonrpc.getheader();
        //准备请求数据
        HashMap<String, Object> data = new HashMap<>();
        data.put("id",1001);
        data.put("method","getblockcount");
        data.put("params",null);
        data.put("json_rpc","2.0");
        // 转为 byte 字节数组
        byte[] bytes = JSONObject.toJSONBytes(data);
        String Result = null;
        try {
            Result = HttpUtils.post(Constants.BITCOIN_RPC_URL, header, bytes);
        } catch (IOException e) {
        }
        // JSON 解析
        Rpcbitcoin rpcbitcoin = JSONObject.parseObject(Result,Rpcbitcoin.class);
        return rpcbitcoin;
    }
}
