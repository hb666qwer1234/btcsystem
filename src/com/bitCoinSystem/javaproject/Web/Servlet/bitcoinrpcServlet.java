package com.bitCoinSystem.javaproject.Web.Servlet;

import com.bitCoinSystem.javaproject.Bean.Rpcbitcoin;
import com.bitCoinSystem.javaproject.Service.GetBestBlockHash;
import com.bitCoinSystem.javaproject.Service.GetBlockCount;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author: nilcity
 */
@WebServlet("/bitcoinrpcServlet")
public class bitcoinrpcServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置编码
        request.setCharacterEncoding("utf-8");
        //  获取参数
        String id = request.getParameter("id");
        if (id.equals("1")){
            //  调用Service 相应的功能
            Rpcbitcoin getbestblockhash = GetBestBlockHash.getbestblockhash();
            if (getbestblockhash.getError()==null){
                request.setAttribute("Result",getbestblockhash.getResult());
                request.setAttribute("Id",getbestblockhash.getId());
            }else{
                request.getRequestDispatcher("/error.jsp").forward(request,response);
                request.setAttribute("error",getbestblockhash.getError());
            }
        }else if (id.equals("2")){
            Rpcbitcoin getblockcount = GetBlockCount.getblockcount();
            if (getblockcount.getError()==null){
                request.setAttribute("Result",getblockcount.getResult());
                request.setAttribute("Id",getblockcount.getId());
            }else{
                request.getRequestDispatcher("/error.jsp").forward(request,response);
                request.setAttribute("error",getblockcount.getError());
            }
        }else{
            request.getRequestDispatcher("/error.jsp").forward(request,response);
        }
        // 转发
        request.getRequestDispatcher("/rpcshow.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
