package com.bitCoinSystem.javaproject.Web.Servlet;

import com.bitCoinSystem.javaproject.Bean.Game;
import com.bitCoinSystem.javaproject.Bean.Hotvideo;
import com.bitCoinSystem.javaproject.Service.GameService;
import com.bitCoinSystem.javaproject.Service.HotvidaoService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author: nilcity
 */
@WebServlet("/listServlet")
public class ListServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置编码
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=UTF-8");
        // 获取参数
        String id = request.getParameter("id");
        if (id.equals( "1")){
            // 调用 Service 查询
            List<Hotvideo> hotvideo = HotvidaoService.GetHotvidaoService();
            // 将hotvideos存入 request域中
            request.setAttribute("hotvideo",hotvideo);
            // 转发到 show.jsp 中
            request.getRequestDispatcher("/show.jsp").forward(request,response);
        }else if (id.equals( "2")){
            List<Game> games = GameService.GetGameService();
            // 将 games 存入 request区域 中
            request.setAttribute("games",games);
            // 转发到 show.jsp 中
            request.getRequestDispatcher("/show1.jsp").forward(request,response);
        }else{
            request.getRequestDispatcher("/error.jsp").forward(request,response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
