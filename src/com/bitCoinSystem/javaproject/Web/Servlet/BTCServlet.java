package com.bitCoinSystem.javaproject.Web.Servlet;

import com.bitCoinSystem.javaproject.bitcoinAddress.Address;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.KeyPair;

/**
 * @author: nilcity
 */
@WebServlet("/BTCServlet")
public class BTCServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置编码
        request.setCharacterEncoding("utf-8");
        // 调用Service
        Address address = new Address();
        KeyPair keyPair = address.GenerateKey();
        String address1 = address.newAddress(keyPair);
        request.setAttribute("address",address1);
        //  转发
        request.getRequestDispatcher("/btcdata.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
